package com.hcl.fao.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.when;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.fao.entity.User;
import com.hcl.fao.entity.UserRequestDto;
import com.hcl.fao.service.IUserService;


@ExtendWith(MockitoExtension.class)
 class UserControllerTest {

	@Mock
	IUserService userService;
	
	@InjectMocks
	UserController userController;
	
	UserRequestDto userRequestDto;
	User user;
	
	@BeforeEach
	void setUp()
	{
		userRequestDto = new UserRequestDto();
		userRequestDto.setUserName("Darshan");
		userRequestDto.setPassword("1234");
		
		user = new User();
		user.setUserId(1);
		user.setUserName("Darshan");
		user.setAddress("Bangalore");
		user.setPassword("1234");
		user.setPhoneNumber(56535673);
		user.setRoles("user");
		user.setActive(false);
	}
	
	
	@Test
	@DisplayName("Register User Test")
	void registerUserTest()
	{
		when(userService.registerUser(user)).thenReturn("User Registered Successfully.!");
		
		ResponseEntity<String> result = userController.registration(user);
		
		assertEquals("User Registered Successfully.!", result.getBody());
		assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
	}
	
	
	@Test
	@DisplayName("Login User Test")
	void loginUserTest()
	{
		when(userService.loginUser(userRequestDto)).thenReturn("Successfully logged In");
		
		
		ResponseEntity<String> result = userController.loginUser(userRequestDto);
		
		assertEquals("Successfully logged In", result.getBody());
		assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
	}
}
