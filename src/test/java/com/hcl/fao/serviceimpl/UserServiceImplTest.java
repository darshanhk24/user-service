package com.hcl.fao.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import com.hcl.fao.entity.User;
import com.hcl.fao.entity.UserRequestDto;
import com.hcl.fao.repo.IUserRepository;


@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

	@Mock
	IUserRepository userRepository;
	
	@InjectMocks
	UserServiceImpl userService;
	
	UserRequestDto userRequestDto1;
	UserRequestDto userRequestDto2;
	User user;
	
	@BeforeEach
	void setUp()
	{
		userRequestDto1 = new UserRequestDto();
		userRequestDto1.setUserName("Darshan");
		userRequestDto1.setPassword("1234");
		
		userRequestDto2 = new UserRequestDto();
		userRequestDto2.setUserName("Darshan");
		userRequestDto2.setPassword("123");
		
		user = new User();
		user.setUserId(1);
		user.setUserName("Darshan");
		user.setAddress("Bangalore");
		user.setPassword("1234");
		user.setPhoneNumber(56535673);
		user.setRoles("user");
		user.setActive(false);
	}
	
	
	@Test
	@DisplayName("Register User Test")
	void registerUserTest()
	{
		when(userRepository.save(any(User.class))).thenAnswer(i -> {
			User user = i.getArgument(0);
			user.setUserId(1);
			return user;
		});
		
		String result = userService.registerUser(user);
		
		assertEquals("User Registered Successfully.!", result);
	}
	
	
	@Test
	@DisplayName("Login User Test Positive")
	void loginUserTest_Positive()
	{
		when(userRepository.findByUserName(userRequestDto1.getUserName())).thenReturn(user);
		
		
		String result = userService.loginUser(userRequestDto1);
		
		assertEquals("Successfully logged In", result);
	}
	
	@Test
	@DisplayName("Login User Test Negative 1")
	void loginUserTest_Negative1()
	{
		when(userRepository.findByUserName(userRequestDto2.getUserName())).thenReturn(user);
		
		
		String result = userService.loginUser(userRequestDto2);
		
		assertEquals("Wrong password.!", result);
	}
	
	@Test
	@DisplayName("Login User Test Negative 2")
	void loginUserTest_Negative2()
	{
		when(userRepository.findByUserName(userRequestDto1.getUserName())).thenReturn(null);
		
		
		String result = userService.loginUser(userRequestDto1);
		
		assertEquals("Invalid Username..! Try logging in with correct username.!", result);
	}
}
