package com.hcl.fao.entity;

import lombok.Data;

@Data
public class UserRequestDto {

    private String userName;
    private String password;
}
