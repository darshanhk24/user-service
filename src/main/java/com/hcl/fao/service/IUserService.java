package com.hcl.fao.service;


import org.springframework.stereotype.Service;

import com.hcl.fao.entity.UserRequestDto;
import com.hcl.fao.entity.User;

@Service
public interface IUserService {

	String registerUser(User user);

	String loginUser(UserRequestDto userRequestDto);

}
