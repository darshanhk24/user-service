package com.hcl.fao.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.fao.entity.User;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer> {

	User findByUserName(String userName);

}
