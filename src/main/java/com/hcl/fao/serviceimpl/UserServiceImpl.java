package com.hcl.fao.serviceimpl;


import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.fao.entity.UserRequestDto;
import com.hcl.fao.entity.User;
import com.hcl.fao.repo.IUserRepository;
import com.hcl.fao.service.IUserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService{

	@Autowired
	IUserRepository userRepo;
	

	@Override
	public String registerUser(User user) {
		userRepo.save(user);
		return "User Registered Successfully.!";
	}


	@Override
	public String loginUser(UserRequestDto userRequestDto) {
		
		User user = userRepo.findByUserName(userRequestDto.getUserName());
		if(user == null)
			return "Invalid Username..! Try logging in with correct username.!";
		
		if(user.getPassword().equals(userRequestDto.getPassword()))
		{
			user.setActive(true);
			return "Successfully logged In";
		}
		else
			return "Wrong password.!";
	}

	

}
