package com.hcl.fao.controller;



import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.fao.entity.User;
import com.hcl.fao.entity.UserRequestDto;
import com.hcl.fao.service.IUserService;

@Validated
@RestController
public class UserController {

	@Autowired
	IUserService userService;
	
	
	@PostMapping("/users")
	public ResponseEntity<String> registration(@Valid @RequestBody User user) {
		
		return new ResponseEntity<>(userService.registerUser(user), HttpStatus.ACCEPTED);
	}
	
	@PostMapping("/user")
	public ResponseEntity<String> loginUser(@Valid @RequestBody UserRequestDto userRequestDto) {
		return new ResponseEntity<>(userService.loginUser(userRequestDto), HttpStatus.ACCEPTED);
	}
}
